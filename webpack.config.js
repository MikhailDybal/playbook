const resolve = require('path').resolve;

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = function (env) {
  return {
    context: resolve('src'),
    entry: './entry.js',
    output: {
      path: resolve('dist'),
      filename: 'bundle.js',
      publicPath: '/dist/',
    },
    devtool: env.prod ? 'source-map' : 'eval',
    module: {
      rules: [
        {
          test: /\.js$/,
          loaders: ['babel-loader'], exclude: /node_modules/
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [{
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            }, {
              loader: 'postcss-loader', // translates CSS into CommonJS
              options: {
                ident: 'postcss',
                sourceMap: true,
                plugins: () => [
                  autoprefixer({ browsers: ['last 2 versions', '> 2%', '> 5% in DE', 'Chrome 44', 'iOS > 8', 'ie > 10'], grid: true }),
                ],
              },
            }, {
              loader: "sass-loader",
              options: {
                sourceMap: true,
                includePaths: [
                  resolve(__dirname, './src/sass'),
                  resolve(__dirname, './src/sass/components'),
                ],
              }
            }]
          })
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          loader: 'file-loader',
          options: {
            name: './fonts/[name].[ext]',
          },
        },
        {
          test: /\.(gif|png|jpe?g|svg)$/i,
          loaders: [
            'file-loader',
            {
              loader: 'image-webpack-loader',
              query: {
                progressive: true,
                optimizationLevel: 7,
                interlaced: false,
                pngquant: {
                  quality: '65-90',
                  speed: 4
                }
              }
            }
          ]
        },
      ],
    },

    plugins: [
      new CleanWebpackPlugin(['dist']),
      new ExtractTextPlugin('styles.css'),
      new CopyWebpackPlugin([
        { from: 'img', to: 'img' },
        { from: 'js', to: 'js' },
        { from: 'lib', to: 'lib' }
      ]),
    ],
  }
}
