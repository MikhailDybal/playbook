var videoID = $("#about").attr("data-video");
if (videoID) {
  var tag = document.createElement('script');
  tag.src = 'https://www.youtube.com/player_api';
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  var tv,
    playerDefaults = {
      autohide: 1,
      autoplay: 0,
      cc_load_policy: 0,
      controls: 0,
      disablekb: 0,
      enablejsapi: 0,
      fs: 0,
      iv_load_policy: 3,
      loop: 1,
      modestbranding: 1,
      rel: 0,
      showinfo: 0,
      playlist: videoID,
      version: 3,
    };
  var vid = { 'videoId': videoID, 'suggestedQuality': 'hd720' }

  function onYouTubePlayerAPIReady() {
    tv = new YT.Player('tv', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange }, playerVars: playerDefaults });
  }

  function onPlayerReady() {
    tv.loadVideoById(vid);
    tv.mute();
    tv.setLoop(true);
  }

  function onPlayerStateChange(e) {
    if (e.data === 1) {
      $('#tv').addClass('active');
    } else if (e.data === 2) {
      $('#tv').removeClass('active');
      tv.loadVideoById(vid);
      tv.seekTo(vid.startSeconds);
    }
    if (e.data === YT.PlayerState.ENDED) {
      tv.playVideo(); 
    }
  }

  function vidRescale() {
    var w = $('#about').width(),
      h = $('#about').innerHeight();
    if (w / h > 16 / 9) {
      tv.setSize(w, w / 16 * 9);
      $('.tv .screen').css({ 'left': '0px' });
    } else {
      tv.setSize(h / 9 * 16, h);
      $('.tv .screen').css({ 'left': -($('.tv .screen').outerWidth() - w) / 2 });
    }
  }

  $(window).on('load resize', function () {
    vidRescale();
  });
}
