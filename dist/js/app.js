(function ($) {
  function initCarousel($slider) {
    var aboutCaroselSettings = {
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      infinite: true,
      cssEase: 'linear',
      speed: 500,
      autoplay: true,
      adaptiveHeight: false,
      autoplaySpeed: 8000,
      prevArrow: false,
      nextArrow: false,
      fade: true,
    }; // end aboutCaroselSettings

    if ($slider.length) {
      var aboutCarousel = $slider.slick(aboutCaroselSettings);

      aboutCarousel.on('mouseover', function () {
        aboutCarousel.slickPause();
      });

      aboutCarousel.on('mouseleave', function () {
        aboutCarousel.slickPlay();
      });

      var isVideoItems = $('.with-video');
      if (isVideoItems && isVideoItems[0]) {
        $('.slick-dots').addClass('with-video');
      }
    }
  } // end initCarousel

  function setDotsWidthAboutCarousel() {
    try {
      var aboutItemWidth = $("#about .about-item-text-container").width();
      $("#about .slick-dots").width(aboutItemWidth);
    } catch (error) { }
  } // end setDotsWidthAboutCarousel

  function setPlaceholders() {
    try {
      $('.palceholder').click(function () {
        $(this).siblings('input').focus();
        $(this).siblings('textarea').focus();
      });
      $('.form-control').focus(function () {
        $(this).siblings('.palceholder').hide();
      });
      $('.form-control').blur(function () {
        var $this = $(this);
        if ($this.val().length == 0)
          $(this).siblings('.palceholder').show();
      });
      $('.form-control').blur();
    } catch (error) { }
  }

  function initWOW() {
    try {
      new WOW().init();
    } catch (error) { }
  }

  function addAnimationScrollToMenu() {
    try {
      $(document).on('click', 'a[href^="#"]', function (e) {
        // target element id
        var id = $(this).attr('href');
        // target element
        var $id = $(id);
        if ($id.length === 0) {
          return;
        }
        // prevent standard hash navigation (avoid blinking in IE)
        e.preventDefault();
        // top position relative to the document
        var pos = $id.offset().top;
        // animated top scrolling
        $('body, html').animate({ scrollTop: pos });
      });
    } catch (error) { }
  }

  $(document).ready(function () {
    initCarousel($('#about .slider'));
    initCarousel($('#reviews .slider'));
    setDotsWidthAboutCarousel();
    setPlaceholders();
    initWOW();
    addAnimationScrollToMenu();
  }); //end ready

  $(window).resize(function () {
    setDotsWidthAboutCarousel()
  });//end resize

})(jQuery);