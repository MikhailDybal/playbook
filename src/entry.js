require('./sass/reset.scss');
require('./sass/fonts.scss');
require('./sass/variables.scss');
require('./sass/mixins.scss');
require('./sass/bootstrap-custom.scss');
require('./sass/main.scss');

// Components
require('./sass/components/start.scss');
require('./sass/components/about.scss');
require('./sass/components/reviews.scss');
require('./sass/components/contact.scss');
require('./sass/components/footer.scss');


require('bootstrap');
